﻿using System;
using Domain.DTOs;
using System.Net.Http;
using Newtonsoft.Json;
using NUnit.Framework;
using FluentAssertions;
using Flurl.Http.Testing;
using Domain.Service.Acl;
using Domain.Interfaces.Acl;

namespace Domain.Tests.ServiceAcl
{
    [TestFixture]
    public class FinancialEntryAclTests
    {
        IFinancialEntryAcl _financialEntryAcl;
        string baseUrl;

        [SetUp]
        public void Setup()
        {
            baseUrl = "http://potato.com";
            _financialEntryAcl = new FinancialEntryAcl();
        }

        [Test]
        public void ShouldEffectiveTransferInunsuccessfully()
        {
            var suffix = "/V1/Lancamento/Inserir";

            var request = new LancamentoDiarioRequest
            {
                IdContaBancaria = 10,
                TipoOperacao = 1,
                TipoLancamento = 1,
                Valor = 10.00,
                Descricao = "Divisão de Despesa com Seu Amigo João",
                IdCategoria = 10,
                DataTransacao = DateTime.Now,
                UniqueId = 13,
                DataCadastro = DateTime.Now,
                Aplicar = 0,
                CodigoMoeda = 939,
                Estornado = false,
                Ocultar = false,
                Regularizacao = false,
                Status = 1
            };

            var response = new LancamentoDiarioResponse { Id = 0 };

            HttpTest httpTest = new HttpTest();

            httpTest.RespondWith(JsonConvert.SerializeObject(response), 200);

            var result = _financialEntryAcl.AccountsTransfer(request);

            result.Should().BeFalse();

            httpTest.ShouldHaveCalled(String.Concat(baseUrl, suffix)).WithVerb(HttpMethod.Post);
        }
    }
}
