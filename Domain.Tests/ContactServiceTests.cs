﻿using Domain.Interfaces;
using Domain.Service;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using Moq;
using Domain.DTOs;

namespace Domain.Tests
{
    [TestFixture]
    public class ContactServiceTests
    {
        [Test]
        public void ShouldListAllContactsByClientId()
        {
            List<Contacts> contacts = new List<Contacts>() { new Contacts() { Id = 2, Name = "a"} };
            Mock<IContactsRepository> rep = new Mock<IContactsRepository>();
            rep.Setup(x => x.ListContacts(120111)).Returns(contacts);
           
            IContactService service = new ContactService(rep.Object);

            List<Contacts> idContacts = service.ListContacts(120111);

            idContacts.Should().NotBeNull();
          
        }
    }
}
