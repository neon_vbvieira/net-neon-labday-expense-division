﻿using Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces.Acl
{
    public interface IFinancialEntryAcl
    {
        bool AccountsTransfer(LancamentoDiarioRequest lancamento);
    }
}
