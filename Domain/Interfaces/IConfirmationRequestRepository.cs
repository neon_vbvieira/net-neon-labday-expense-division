﻿using Domain.DTOs;
using Domain.Entities;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IConfirmationRequestRepository
    {
        Task<int> SaveFinancialEntry(TransitionRequest transitionRequest);

        Task SaveFinancialEntryFavored(ExpenseDivisionFavored transition);

        Task<decimal> GetValueFinancialEntry(int financialEntryId);
    }
}