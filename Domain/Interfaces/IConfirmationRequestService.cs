﻿using Domain.DTOs;

namespace Domain.Interfaces
{
    public interface IConfirmationRequestService
    {
        void SendTransition(TransitionRequest transition);
    }
}