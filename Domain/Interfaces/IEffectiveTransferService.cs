﻿using Domain.DTOs;

namespace Domain.Interfaces
{
    public interface IEffectiveTransferService
    {
         bool AccountsTransfer(LancamentoDiarioResponse lancamento);
    }
}
