﻿using Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface IContactsRepository
    {

        List<Contacts> ListContacts(int client);

    }
}
