﻿using Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface IContactService
    {
        List<Contacts> ListContacts(int clientId);
    }
}
