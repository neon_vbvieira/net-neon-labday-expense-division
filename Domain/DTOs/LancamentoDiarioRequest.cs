﻿using System;

namespace Domain.DTOs
{
    public class LancamentoDiarioRequest
    {
        public int IdContaBancaria { get; set; }
        public int TipoOperacao { get; set; }
        public int TipoLancamento { get; set; }
        public double Valor { get; set; }
        public string Descricao { get; set; }
        public int IdCategoria { get; set; }
        public DateTime DataTransacao { get; set; }
        public int UniqueId { get; set; }
        public DateTime DataCadastro { get; set; }
        public int Aplicar { get; set; }
        public int CodigoMoeda { get; set; }
        public bool Estornado { get; set; }
        public bool Ocultar { get; set; }
        public bool Regularizacao { get; set; }
        public int Status { get; set; }
    }
}
