﻿using System.Collections.Generic;

namespace Domain.DTOs
{
    public class TransitionRequest
    {
        public int ClientId { get; set; }
        public int FinancialEntryId { get; set; }
        public IEnumerable<int> FavoredClientId { get; set; }
    }
}