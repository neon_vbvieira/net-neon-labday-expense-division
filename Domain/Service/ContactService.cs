﻿using Domain.DTOs;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Service
{
    public class ContactService : IContactService
    {
        private IContactsRepository _contactsRepository;

        public ContactService(IContactsRepository contactsRepository)
        {
            this._contactsRepository = contactsRepository;
        }


        public List<Contacts> ListContacts(int clientId)
        {
            List<Contacts> contacts = _contactsRepository.ListContacts(clientId);

            return contacts;
        }
    }
}
