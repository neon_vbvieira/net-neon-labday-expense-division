﻿using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;

namespace Domain.Service.Acl.Base
{
    public class ServiceAclBase
    {
        protected T RequestBase<T>(string suffix, HttpMethod httpVerb, object request = null, string parameters = "", [CallerMemberName]string Name = "")
        {
            string baseUrl = "";//_configurationProvider.GetAs<string>("neon.datacob.url");
            //suffix = _configurationProvider.GetAs<string>(suffix);

            try
            {
                
                //string user = _configurationProvider.GetAs<string>("neon.datacob.user");
                //string password = _configurationProvider.GetAs<string>("neon.datacob.password");
                //string apiKey = _configurationProvider.GetAs<string>("neon.datacob.apikey");

                if (HasValidParameters(baseUrl, suffix))
                {
                    suffix = string.IsNullOrEmpty(parameters) ? suffix : suffix + parameters;
                    T result = default(T);

                    if (httpVerb.Equals(HttpMethod.Post))
                    {
                        result = baseUrl.AppendPathSegment(suffix).PostJsonAsync(request).ReceiveJson<T>().Result;
                    }
                   

                    return result;
                }

                
            }
            catch (Exception ex)
            {
                //Implementar LOG
            }
            return default(T);
        }

        protected static bool HasValidParameters(string baseUrl, string suffix)
        {
            return !string.IsNullOrEmpty(baseUrl) && !string.IsNullOrEmpty(suffix);
        }
    }
}
