﻿using Domain.DTOs;
using Domain.Interfaces.Acl;
using Domain.Service.Acl.Base;
using System.Net.Http;

namespace Domain.Service.Acl
{
    public class FinancialEntryAcl : ServiceAclBase, IFinancialEntryAcl
    {
        public bool AccountsTransfer(LancamentoDiarioRequest lancamento)
        {
            var result = RequestBase<LancamentoDiarioResponse>("http://10.36.21.24", HttpMethod.Post, lancamento);

            if (result.Id < 0)
                return false;

            return true;
        }
    }
}
