﻿using Domain.DTOs;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Service
{
    public class ConfirmationRequestService : IConfirmationRequestService
    {
        private readonly IConfirmationRequestRepository _confirmationRequest;

        public ConfirmationRequestService(IConfirmationRequestRepository confirmationRequest)
        {
            _confirmationRequest = confirmationRequest;
        }

        public async void SendTransition(TransitionRequest transition)
        {
            var expenseDivisionId = await _confirmationRequest.SaveFinancialEntry(transition);
            var financialValue = await GetValueFinancialEntryPerFavored(transition.FinancialEntryId, transition.FavoredClientId.Count());

            foreach (var clientId in transition.FavoredClientId)
            {
                ExpenseDivisionFavored clientFavored = new ExpenseDivisionFavored
                {
                    ExpenseDivision = new ExpenseDivision
                    {
                        Id = expenseDivisionId
                    },
                    FinancialValue = financialValue,
                    FavoredClientId = clientId
                };
                await _confirmationRequest.SaveFinancialEntryFavored(clientFavored);
            }
        }

        private async Task<decimal> GetValueFinancialEntryPerFavored(int financialEntryId, int favoredAmount)
        {
            var financialValue = await _confirmationRequest.GetValueFinancialEntry(financialEntryId);

            return Math.Round(financialValue / favoredAmount, 2);
        }
    }
}