﻿using System;

namespace Domain.Entities
{
    public class ExpenseDivision
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int FinancialEntryId { get; set; }
        public DateTime RegisterDate { get; set; }
    }
}