﻿using System;

namespace Domain.Entities
{
    public class ExpenseDivisionFavored
    {
        public ExpenseDivision ExpenseDivision { get; set; }
        public int FavoredClientId { get; set; }
        public decimal FinancialValue { get; set; }
        public bool Approved { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}