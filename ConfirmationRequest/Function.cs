// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
using Amazon.Lambda.Core;
using Domain.DTOs;
using Domain.Interfaces;
using Domain.Service;
using Microsoft.Extensions.DependencyInjection;
using Repository;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace ConfirmationRequest
{
    public class Function
    {
        public void FunctionHandler(TransitionRequest transition, ILambdaContext context)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            var serviceProvider = serviceCollection.BuildServiceProvider();

            serviceProvider.GetService<App>().Run(transition);
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IConfirmationRequestService, ConfirmationRequestService>();
            serviceCollection.AddScoped<IConfirmationRequestRepository, ConfirmationRequestRepository>();

            // here is where you're adding the actual application logic to the collection
            serviceCollection.AddTransient<App>();
        }
    }

    public class App
    {
        private readonly IConfirmationRequestService _service;

        public App(IConfirmationRequestService service)
        {
            _service = service;
        }

        public void Run(TransitionRequest transition)
        {
            _service.SendTransition(transition);
        }
    }
}