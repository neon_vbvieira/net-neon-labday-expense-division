﻿using Dapper;
using Domain.DTOs;
using Domain.Entities;
using Domain.Interfaces;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Repository
{
    public class ConfirmationRequestRepository : IConfirmationRequestRepository
    {
        private readonly string connectionString = @"Data Source=10.36.21.33,2500\sqlneondes01;Initial Catalog=NeonPottencial;Integrated Security=False;User ID=neondev;Password=Neon@357;";

        public async Task<int> SaveFinancialEntry(TransitionRequest transitionRequest)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                return await connection.QueryFirstAsync<int>(@"declare @expenseDivisionId int
                                                            insert into ExpenseDivision values(
                                                            @clientId, @financialEntryId, getdate())
                                                            set @expenseDivisionId = SCOPE_IDENTITY()",
                                                            new
                                                            {
                                                                transitionRequest.ClientId,
                                                                transitionRequest.FinancialEntryId
                                                            }, commandType: CommandType.Text);
            }
        }

        public async Task SaveFinancialEntryFavored(ExpenseDivisionFavored transition)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                await connection.ExecuteAsync(@"insert into ExpenseDivisionFavored
                                                (ExpenseDivisionId, FavoredClientId, FinancialValue, UpdateDate)
                                                values(@ExpenseDivisionId, @FavoredClientId, @FinancialValue, getdate())",
                                                new
                                                {
                                                    transition.ExpenseDivision,
                                                    transition.FavoredClientId,
                                                    transition.FinancialValue
                                                }, commandType: CommandType.Text);
            }
        }

        public async Task<decimal> GetValueFinancialEntry(int financialEntryId)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                return await connection.QueryFirstAsync<decimal>(@"select value from LancamentoDiario
                                                                    where id = @financialEntryId",
                                                                    new { financialEntryId }, commandType: CommandType.Text);
            }
        }
    }
}