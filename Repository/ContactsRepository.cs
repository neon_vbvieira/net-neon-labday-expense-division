﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Dapper;
using Domain.DTOs;

namespace Repository
{
    public class ContactsRepository : IContactsRepository
    {
        public List<Contacts> ListContacts(int clientId)
        {
            string connectionString = @"Data Source=10.36.21.33,2500\sqlneondes01;Initial Catalog=NeonPottencial;Integrated Security=False;User ID=neondev;Password=Neon@357;";
            
            using (var connection = new SqlConnection(connectionString))
            {
                string sql = @"
                                SELECT ClientIDNeon as Id, [Name]
                                FROM ClientTranfersContacts(NOLOCK)
                                WHERE ClientIDNeon != 0 AND clientid = @clientId;";

               
                var result = connection.Query<Contacts>(sql, new { clientId }, commandType: System.Data.CommandType.Text);

                return result.AsList<Contacts>();
            }
        }
    }
}
