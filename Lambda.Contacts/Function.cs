using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Amazon.Lambda.Core;
using Domain.Interfaces;
using Domain.Service;
using Repository;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Lambda.Contacts
{
    public class Function
    {
        public List<Domain.DTOs.Contacts> FunctionHandler(int clientId, ILambdaContext context)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            var serviceProvider = serviceCollection.BuildServiceProvider();

            return serviceProvider.GetService<App>().Run(clientId);
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IContactService, ContactService>();
            serviceCollection.AddScoped<IContactsRepository, ContactsRepository>();

            // here is where you're adding the actual application logic to the collection
            serviceCollection.AddTransient<App>();
        }
    }

    public class App
    {
        private readonly IContactService _service;

        public App(IContactService service)
        {
            _service = service;
        }

        public List<Domain.DTOs.Contacts> Run(int clientId)
        {
            var response = _service.ListContacts(clientId);

            return response;
        }
    }

}
