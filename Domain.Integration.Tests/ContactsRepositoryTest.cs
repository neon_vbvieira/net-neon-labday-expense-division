using Domain.Interfaces;
using NUnit.Framework;
using Repository;
using System.Collections.Generic;
using FluentAssertions;
using Domain.DTOs;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void ShouldListAllClientContacts()
        {
            IContactsRepository rep = new ContactsRepository();

            List<Contacts> resp = rep.ListContacts(120111);

            resp.Should().NotBeNull();
        }
    }
}