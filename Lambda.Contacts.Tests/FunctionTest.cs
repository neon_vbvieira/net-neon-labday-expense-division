using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using Lambda.Contacts;
using FluentAssertions;

namespace Lambda.Contacts.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestToUpperFunction()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            var contact = function.FunctionHandler(120111, context);

            contact.Count.Should().BeGreaterThan(0);
        }
    }
}
